Source code for BostonGene Junior task #1 solved by Nikolay Ilin.

Maven based project.

# Getting started
Compile the application sources:
```sh
mvn compile
```
Create a JAR file:
```sh
mvn package
```
Run the application:
```sh
java -jar MultithreadingTask.jar
```
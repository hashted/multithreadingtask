public class RemoverThread extends Thread {
    private SharedCollection collection;

    public  RemoverThread(SharedCollection collection) {
        this.collection = collection;
    }

    @Override
    public void run() {
        while(true) {
            try {
                Thread.sleep(5000);

                if (collection.getMap().size() > 0) {
                    String removed = collection.removeFirst();
                    System.out.print("\nThread 2: Removed '" + removed + "'. Your collection: " + collection.getMap() + "\n> ");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

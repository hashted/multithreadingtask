import java.util.InputMismatchException;
import java.util.Scanner;

public class CollectorThread extends Thread {

    private SharedCollection collection;

    public  CollectorThread(SharedCollection collection) {
        this.collection = collection;
    }

    @Override
    public void run() {
        System.out.println("Thread 1: Please enter your number from 'one' to 'nine thousand nine hundred ninety nine': ");
        Scanner in = new Scanner(System.in);

        String currentLine;

        while (true) {
            System.out.print("> ");
            currentLine = in.nextLine();

            int num = NumberConverter.wordsToInt(currentLine);
            if (num != -1) {
                collection.add(num, currentLine);
                System.out.println("Thread 1: Added. Your collection: " + collection.getMap());
            } else {
                System.out.println("Thread 1: Invalid input. Please try again.");
            }
        }
    }
}

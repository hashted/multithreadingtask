import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class MultithreadingApp {

    public static void main(String[] args) {

        SharedCollection sharedCollection = new SharedCollection();

        Thread threadOne = new CollectorThread(sharedCollection);
        Thread threadTwo = new RemoverThread(sharedCollection);
        threadOne.start();
        threadTwo.start();
    }
}

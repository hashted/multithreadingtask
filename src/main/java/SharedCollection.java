import java.util.TreeMap;

public class SharedCollection {
    private TreeMap<Integer, String> map = new TreeMap<>();
    private boolean ready = false;

    public TreeMap<Integer, String> getMap() {
        return map;
    }

    public void add(Integer key, String str) {
        synchronized (map) {
            map.put(key, str);
            ready = true;
            map.notify();
        }
    }

    public String removeFirst() {
        synchronized (map) {
            while(!ready) {
                try {
                    map.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            int firstKey = map.firstKey();
            String str = map.get(firstKey);
            map.remove(firstKey);
            return str;
        }
    }
}
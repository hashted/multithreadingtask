import java.util.Arrays;
import java.util.List;

public class NumberConverter {

    static List<String> dictionary = Arrays.asList(
            "one","two","three","four","five","six","seven", "eight","nine","ten",
            "eleven","twelve","thirteen","fourteen", "fifteen","sixteen","seventeen","eighteen","nineteen","twenty",
            "thirty","forty","fifty","sixty","seventy","eighty","ninety", "hundred","thousand"
    );

    static int wordsToInt(String str) {

        int temp = 0;
        int result = 0;

        if (str != null && str.length() > 0) {

            str = str.replace("-", " ");
            str = str.replace(" and ", " ");
            str = str.toLowerCase().trim();

            String[] components = str.split("\\s+");

            for(String c : components) {
                if(!dictionary.contains(c)) {
                    return -1;
                }

                else if(c.equals("one")) {
                    temp += 1;
                }
                else if(c.equals("two")) {
                    temp += 2;
                }
                else if(c.equals("three")) {
                    temp += 3;
                }
                else if(c.equals("four")) {
                    temp += 4;
                }
                else if(c.equals("five")) {
                    temp += 5;
                }
                else if(c.equals("six")) {
                    temp += 6;
                }
                else if(c.equals("seven")) {
                    temp += 7;
                }
                else if(c.equals("eight")) {
                    temp += 8;
                }
                else if(c.equals("nine")) {
                    temp += 9;
                }
                else if(c.equals("ten")) {
                    temp += 10;
                }
                else if(c.equals("eleven")) {
                    temp += 11;
                }
                else if(c.equals("twelve")) {
                    temp += 12;
                }
                else if(c.equals("thirteen")) {
                    temp += 13;
                }
                else if(c.equals("fourteen")) {
                    temp += 14;
                }
                else if(c.equals("fifteen")) {
                    temp += 15;
                }
                else if(c.equals("sixteen")) {
                    temp += 16;
                }
                else if(c.equals("seventeen")) {
                    temp += 17;
                }
                else if(c.equals("eighteen")) {
                    temp += 18;
                }
                else if(c.equals("nineteen")) {
                    temp += 19;
                }
                else if(c.equals("twenty")) {
                    temp += 20;
                }
                else if(c.equals("thirty")) {
                    temp += 30;
                }
                else if(c.equals("forty")) {
                    temp += 40;
                }
                else if(c.equals("fifty")) {
                    temp += 50;
                }
                else if(c.equals("sixty")) {
                    temp += 60;
                }
                else if(c.equals("seventy")) {
                    temp += 70;
                }
                else if(c.equals("eighty")) {
                    temp += 80;
                }
                else if(c.equals("ninety")) {
                    temp += 90;
                }
                else if(c.equals("hundred")) {
                    temp *= 100;
                }
                else if(c.equals("thousand")) {
                    temp *= 1000;
                    result += temp;
                    temp = 0;
                }
            }
            result += temp;

        } else {
            result = -1;
        }

        return result;
    }
}
